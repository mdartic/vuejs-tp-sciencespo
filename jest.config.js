module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  testMatch: [
    '**/*.spec.(js|jsx|ts|tsx)'
  ],
  'collectCoverage': true,
  'collectCoverageFrom': ['**/*.{js,vue}', '!**/node_modules/**'],
  'coverageReporters': ['html', 'text-summary'],
  transform: {
    '^.+\\.svg$': '<rootDir>/svgTransform.js'
  }

}
