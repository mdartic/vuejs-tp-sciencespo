import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import TableViewDistributions from '../views/TableViewDistributions.vue'

import store from '../store/simpleStore'

Vue.use(VueRouter)

export const ROUTE_HOME = 'home'
export const ROUTE_DISTRIBUTIONS = 'distributions'
export const ROUTE_DISTRIBUTIONS_DETAIL = 'distributionsDetail'
export const ROUTE_MEMBERS = 'members'

export const DEFAULT_ROUTE_NOT_AUTH = ROUTE_HOME
export const DEFAULT_ROUTE_AUTH = ROUTE_DISTRIBUTIONS

export function createRouter () {
  const routes = [
    {
      path: '/home',
      name: ROUTE_HOME,
      component: Home,
      meta: {
        needAuth: false
      }
    },
    {
      path: '/distributions',
      name: ROUTE_DISTRIBUTIONS,
      component: TableViewDistributions,
      meta: {
        needAuth: true
      }
    },
    {
      path: '/distributions/:currentId',
      name: ROUTE_DISTRIBUTIONS_DETAIL,
      component: TableViewDistributions,
      props: true,
      meta: {
        needAuth: true
      }
    },
    {
      path: '/members',
      name: ROUTE_MEMBERS,
      component: () => import(/* webpackChunkName: "TableView" */ '../views/TableView.vue'),
      meta: {
        needAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
  ]

  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })

  router.beforeEach((to, from, next) => {
    // if root, redirect to default route
    if (to.path === '/') {
      if (store.state.auth.isAuth) {
        next({ name: DEFAULT_ROUTE_AUTH })
      } else {
        next({ name: DEFAULT_ROUTE_NOT_AUTH })
      }
    } else {
      // we check if the route ( need | or not ) auth
      if (
        (to.meta.needAuth && store.state.auth.isAuth) ||
            (!to.meta.needAuth && !store.state.auth.isAuth)
      ) {
        next()
      } else if (to.meta.needAuth) {
        // if we are here, user is not auth, we redirect to login
        next({
          name: DEFAULT_ROUTE_NOT_AUTH,
          query: { redirect: to.fullPath }
        })
      } else if (!to.meta.needAuth) {
        // if we are here, user is already auth, we redirect to ideas
        next({ name: DEFAULT_ROUTE_AUTH })
      } else {
        // nothing to do, just call next
        next()
      }
    }
  })

  return router
}
