const store = {
  state: {
    distributions: {
      loading: false,
      result: [],
      error: null
    },
    members: {
      loading: false,
      result: [],
      error: null
    },
    auth: {
      isAuth: true
    }
  },
  actions: {
    toggleAuth () {
      store.state.auth.isAuth = !store.state.auth.isAuth
    }
  }
}

export default store
