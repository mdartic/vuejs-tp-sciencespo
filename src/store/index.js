import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    state: {
      distributions: {
        loading: false,
        result: [],
        error: null
      },
      members: {
        loading: false,
        result: [],
        error: null
      },
      auth: {
        isAuth: false
      }
    },
    mutations: {
      toggleAuth (state) {
        state.auth.isAuth = !state.auth.isAuth
      }
    },
    actions: {
      toggleAuth (context) {
        context.commit('toggleAuth')
      }
    },
    modules: {
      // here we could move distributions / members / auth state + actions + mutations
    }
  })
}
