import { mount, shallowMount } from '@vue/test-utils'
import Table from './Table.vue'
import mockMembers from './mock__members.json'

const columnsConfig1 = [
  {
    text: 'Last name',
    name: 'last_name',
    isSortable: true
  },
  {
    text: 'First name',
    name: 'first_name',
    isSortable: true
  },
  {
    text: 'Email',
    name: 'email',
    extraClasses: ['text-uppercase', 'text-center'],
    isSortable: true
  }
  // {
  //   text: 'Language',
  //   name: 'language',
  //   isSortable: true,
  //   extraClasses: ['text-uppercase'],
  //   component: Badge
  // },
  // {
  //   text: 'State',
  //   name: 'state_label',
  //   isSortable: true,
  //   component: Badge
  // },
  // {
  //   text: 'Qualtrics',
  //   name: 'qualtrics',
  //   isSortable: true,
  //   component: Boolean,
  //   props: {
  //     icons: [SyncIcon, SyncCheckIcon]
  //   }
  // },
  // {
  //   text: 'Edit',
  //   name: 'edit',
  //   component: IconEdit,
  //   props: {
  //     action: 'edit'
  //   }
  // }
]

const columnsConfig2 = [
  {
    text: 'Created on',
    name: 'created',
    isSortable: true
  },
  {
    text: 'Distribution',
    name: 'type',
    isSortable: true
  },
  {
    text: 'Status',
    name: 'is_published',
    isSortable: true
  }
  // {
  //   text: 'Show Children',
  //   names: ['id', 'has_children'],
  //   component: Toggle
  // }
]

// write some tests for sorting
// and check it's ok
describe('Table.vue', () => {
  it('renders correctly', () => {
    const wrapper = mount(Table, {
      propsData: {
        config: columnsConfig1,
        isLoading: false,
        values: mockMembers
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })

  it('renders correctly with shallow mount', () => {
    const wrapper = shallowMount(Table, {
      propsData: {
        config: columnsConfig1,
        isLoading: false,
        values: mockMembers
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })

  it('renders correctly when loading', () => {
    const wrapper = mount(Table, {
      propsData: {
        config: columnsConfig1,
        isLoading: true,
        values: mockMembers
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })

  it('sort correctly data when I click on the column', async () => {
    const wrapper = mount(Table, {
      propsData: {
        config: columnsConfig1,
        isLoading: false,
        values: mockMembers
      }
    })
    expect(wrapper.vm.sortKey).toBe('')
    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': -1, 'first_name': -1, 'last_name': -1 })
    const thEmailArray = wrapper.findAll('th')
    expect(thEmailArray.length).toBe(3)
    expect(thEmailArray.at(2).text()).toBe('Email')
    // now, we sort by email order, but it's already in the good order
    thEmailArray.at(2).trigger('click')
    expect(wrapper.vm.sortKey).toBe('email')
    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': 1, 'first_name': -1, 'last_name': -1 })
    expect(wrapper.vm.sortedValues).toStrictEqual(mockMembers)
    thEmailArray.at(2).trigger('click')
    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': -1, 'first_name': -1, 'last_name': -1 })
    expect(wrapper.vm.sortKey).toBe('email')

    expect(wrapper.vm.sortedValues).toStrictEqual([ { id: 3,
      last_name: 'Third',
      first_name: 'John',
      email: 'John.third@example.com',
      language: 'EN',
      qualtrics: true,
      state_label: [ 'Active', 'badge-success' ],
      edit: '/m/members/3/update/' },
    { id: 2,
      last_name: 'Second',
      first_name: 'John',
      email: 'John.second@example.com',
      language: 'EN',
      qualtrics: true,
      state_label: [ 'Active', 'badge-success' ],
      edit: '/m/members/2/update/' },
    { id: 1,
      last_name: 'First',
      first_name: 'John',
      email: 'John.first@example.com',
      language: 'FR',
      qualtrics: false,
      state_label: [ 'Not notified', 'badge-warning' ],
      edit: '/m/members/1/update/' }
    ])

    expect(wrapper.element).toMatchSnapshot()
  })

  it('sort correctly data when I tap Tabulation three times, and space', async () => {
    const wrapper = mount(Table, {
      propsData: {
        config: columnsConfig1,
        isLoading: false,
        values: mockMembers
      }
    })
    expect(wrapper.vm.sortKey).toBe('')
    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': -1, 'first_name': -1, 'last_name': -1 })

    const thEmailArray = wrapper.findAll('th')
    expect(thEmailArray.length).toBe(3)
    expect(thEmailArray.at(2).text()).toBe('Email')

    // https://github.com/vuejs/vue-test-utils/issues/966
    // https://github.com/jsdom/jsdom/issues/2102

    thEmailArray.at(2).trigger('keyup.space')

    // now, we sort by email order, but it's already in the good order
    expect(wrapper.vm.sortKey).toBe('email')
    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': 1, 'first_name': -1, 'last_name': -1 })
    expect(wrapper.vm.sortedValues).toStrictEqual(mockMembers)

    thEmailArray.at(2).trigger('keyup.space')

    expect(wrapper.vm.sortOrders).toStrictEqual({ 'email': -1, 'first_name': -1, 'last_name': -1 })
    expect(wrapper.vm.sortKey).toBe('email')

    expect(wrapper.vm.sortedValues).toStrictEqual([ { id: 3,
      last_name: 'Third',
      first_name: 'John',
      email: 'John.third@example.com',
      language: 'EN',
      qualtrics: true,
      state_label: [ 'Active', 'badge-success' ],
      edit: '/m/members/3/update/' },
    { id: 2,
      last_name: 'Second',
      first_name: 'John',
      email: 'John.second@example.com',
      language: 'EN',
      qualtrics: true,
      state_label: [ 'Active', 'badge-success' ],
      edit: '/m/members/2/update/' },
    { id: 1,
      last_name: 'First',
      first_name: 'John',
      email: 'John.first@example.com',
      language: 'FR',
      qualtrics: false,
      state_label: [ 'Not notified', 'badge-warning' ],
      edit: '/m/members/1/update/' }
    ])
    expect(wrapper.element).toMatchSnapshot()
  })
})
