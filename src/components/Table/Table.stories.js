/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import mockMembers from './mock__members.json'
import mockDistributions from './mock__distributions.json'

import Table from './Table.vue'
import IconEdit from '../ui/icons/IconEdit.vue'
import Badge from '../ui/Badge/Badge.vue'
import Toggle from '../ui/Toggle/Toggle.vue'
import SyncIcon from '@/styles/icons/sync.svg'
import SyncCheckIcon from '@/styles/icons/sync_check.svg'

const columnsConfig1 = [
  {
    text: 'Last name',
    name: 'last_name',
    isSortable: true
  },
  {
    text: 'First name',
    name: 'first_name',
    isSortable: true
  },
  {
    text: 'Email',
    name: 'email',
    extraClasses: ['text-uppercase', 'text-center'],
    isSortable: true
  },
  {
    text: 'Language',
    name: 'language',
    isSortable: true,
    extraClasses: ['text-uppercase'],
    component: Badge
  },
  {
    text: 'State',
    name: 'state_label',
    isSortable: true,
    component: Badge
  },
  {
    text: 'Qualtrics',
    name: 'qualtrics',
    isSortable: true,
    component: Boolean,
    props: {
      icons: [SyncIcon, SyncCheckIcon]
    }
  },
  {
    text: 'Edit',
    name: 'edit',
    component: IconEdit,
    props: {
      action: 'edit'
    }
  }
]

const columnsConfig2 = [
  {
    text: 'Created on',
    name: 'created',
    isSortable: true
  },
  {
    text: 'Distribution',
    name: 'type',
    isSortable: true
  },
  {
    text: 'Status',
    name: 'is_published',
    isSortable: true
  },
  {
    text: 'Show Children',
    names: ['id', 'has_children'],
    component: Toggle
  }
]

storiesOf('Table', module)
  .add('default', () => ({
    components: { Table },
    render () {
      return <Table
        config={columnsConfig1}
        values={mockMembers}
        isLoading={false}
      />
    }
  }))
  .add('loading', () => ({
    components: { Table },
    render () {
      return <Table
        config={columnsConfig1}
        values={mockMembers}
        isLoading={true}
      />
    }
  }))
  .add('with toggle', () => ({
    components: { Table },
    render () {
      return <Table
        config={columnsConfig2}
        values={mockDistributions.invites}
        isLoading={false}
        onToggle={action('toggle')}
      />
    }
  }))
  // .add('with some emoji', () => ({
  //   components: { Table },
  //   template: '<my-button @click="action">😀 😎 👍 💯</my-button>',
  //   methods: { action: action('clicked') }
  // }))
