/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import Loader from './Loader.vue'

storiesOf('Loader', module)
  .add('default', () => ({
    components: { Loader },
    render () {
      return <Loader />
    }
  }))
