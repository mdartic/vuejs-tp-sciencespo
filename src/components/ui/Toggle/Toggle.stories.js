/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import Toggle from './Toggle.vue'

storiesOf('Toggle', module)
  .add('default', () => ({
    components: { Toggle },
    render () {
      return <Toggle />
    }
  }))
  .add('expanded', () => ({
    components: { Toggle },
    render () {
      return <Toggle expanded />
    }
  }))
