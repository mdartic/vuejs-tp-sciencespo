/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import Boolean from './Boolean.vue'

storiesOf('Boolean', module)
  .add('default', () => ({
    components: { Boolean },
    render () {
      return <Boolean />
    }
  }))
  .add('enabled', () => ({
    components: { Boolean },
    render () {
      return <Boolean value={true} />
    }
  }))
  .add('disabled', () => ({
    components: { Boolean },
    render () {
      return <Boolean value={false} />
    }
  }))
