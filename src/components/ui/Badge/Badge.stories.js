/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import Badge from './Badge.vue'

storiesOf('Badge', module)
  .add('default', () => ({
    components: { Badge },
    render () {
      return <Badge
        value="fr"
      />
    }
  }))
  .add('with array in value field', () => ({
    components: { Badge },
    render () {
      return <Badge
        value={['fr', 'text-uppercase']}
      />
    }
  }))
