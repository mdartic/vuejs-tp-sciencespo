/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import Flag from './Flag.vue'

storiesOf('Flag', module)
  .add('default', () => ({
    components: { Flag },
    render () {
      return <Flag />
    }
  }))
  .add('fr', () => ({
    components: { Flag },
    render () {
      return <Flag
        countryCode="fr"
      />
    }
  }))
