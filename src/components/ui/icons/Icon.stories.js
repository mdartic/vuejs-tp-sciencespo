/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import Icon from './Icon.vue'
import IconEdit from './IconEdit.vue'

storiesOf('Icon', module)
  .add('default', () => ({
    components: { Icon },
    render () {
      return <Icon />
    }
  }))

  .add('edit', () => ({
    components: { IconEdit },
    render () {
      return <IconEdit />
    }
  }))
